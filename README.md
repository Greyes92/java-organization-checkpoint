# Java Organization Checkpoint

# Checkpoint

Verify your understanding of Java organization with the following exercise.

## Functional requirements

Create an application that:

- Takes two command arguments:
    - The first argument is a "booking code" and looks like this: `r102-10:30am-11:00am`.
    - The second argument is "format" and will either be `json`, `csv`, or `html`.
- Print a formatted booking (as seen below)

### Booking Codes:

The booking code looks like this:

```
[room type][room-number]-[start time]-[end time]
```

The room types are:

| Abbreviation   | Room Type       |
| :------------- | :-------------- |
| `r`            | Conference Room |
| `s`            | Suite           |
| `a`            | Auditorium      |
| `c`            | Classroom       |

So the booking code `a14-8:30am-9:00am` would mean:

- An Auditorium (`a`)
- Number `14`
- Starting at `8:30am`
- Ending at `9:00am`

### Formatting:

If the application receives `r111-08:30am-11:00am html` it should print:

```html
<dl>
  <dt>Type</dt><dd>Conference Room</dd>
  <dt>Room Number</dt><dd>111</dd>
  <dt>Start Time</dt><dd>08:30am</dd>
  <dt>End Time</dt><dd>11:00am</dd>
</dl>
```

If the application receives `s111-08:30am-11:00am json` it should print:

```json
{
  "type": "Suite",
  "roomNumber": 111,
  "startTime": "08:30am",
  "endTime": "11:00am"
}
```

If the application receives `a111-08:30am-11:00am csv` it should output:

```
type,room number,start time,end time
Auditorium,111,08:30am,11:00am
```

NOTE:

- You do not need to worry about invalid input.
- You do not need to worry about Date parsing - you may treat dates like Strings.

## Technical requirements

Because this checkpoint is assessing your ability to _organize_ code, you must follow these guidelines:

#### Formatters

Create a package named `formatters`, and within that package:

- Create an Interface named `Formatter`.
- The Formatter interface should have a single method named `format` that:
    - Takes a Booking class as a parameter
    - Returns a String
- You must have three concrete classes that implement the `Formatter` interface:
    - `JSONFormatter`
    - `HTMLFormatter`
    - `CSVFormatter`
- Each of those classes should return the correctly formatted strings (see above)

#### Booking Class

In the main `com.galvanize` package you should define a class named `Booking`, and the `Booking` class should have 4 parts:

1. An enum that represents the 4 Room Types
2. A static method named `.parse` that:
- takes in a booking code
- returns an instance of Booking object
3. A constructor that takes 4 arguments:
    1. Room Type (an `enum` value)
    2. Room Number (a `String`)
    3. Start Time (a `String`)
    4. End Time (a `String`)
4. Getters for all 4 fields

### Application

Your application should be simple, and should use the Booking class and Formatters to do the heavy lifting.

It must have a static method named `getFormatter` that:
- takes what the user provides as the format string ("json", "html" etc...)
- and returns an instance of the correct formatter

The final result should look like this:

![](images/organization-data-model.png)

### Tests

- Write tests in `src/test`.
- Make sure that your test coverage is 90% or higher.




